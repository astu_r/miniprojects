//
//  ViewController.swift
//  mini_Api
//
//  Created by Astu Ranjan on 30/06/21.
//


import UIKit

class ViewController : UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var phoneEntry: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = UIImage(named: "phoneEntry")
    }
    
@IBAction func next(_ sender: Any) {
    
    let num = phoneEntry.text
    if num == ""{
        return
    }
    let firstLetter = num![num!.startIndex]
    if num?.count == 10 && (firstLetter == "6" || firstLetter == "7" || firstLetter=="8" || firstLetter=="9"){
        let getData  = Singleton_API.shared
        ///  NEED TO PUT CALLBACK ....
        getData.getOtp(num: num as Any)
        sleep(1)
        
        let otpView = self.storyboard?.instantiateViewController(identifier: "otpView") as! OtpView
        otpView.otp = getData.otp
       
        print("otp is - \(String(describing: getData.otp))")
        self.navigationController?.pushViewController(otpView, animated: true)
    }
    else{
        return
     }
  }
    
    
    
    
    
//    func getOtp(num : Any){
//
//        let json: [String: Any] = ["phone": num as Any]
//
//    /// convert dictionary to json data ??
//    let jsonData = try? JSONSerialization.data(withJSONObject: json)
//
//    // create post request
//
//        let url = URL(string: ("http://52.66.119.84:2025/api/v1/login/user?debug=true").replacingOccurrences(of: " ", with: "%20"))!
//                var request = URLRequest(url: url)
//                request.httpMethod = "POST"
//                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//
//    // insert json data to the request
//    request.httpBody = jsonData
//
//
//
//
//
//    /// the data is the data we have got in the response to the post request ???
//    let task = URLSession.shared.dataTask(with: request) { data, response, error in
//        guard let data = data, error == nil else {
//            print(error?.localizedDescription ?? "No data")
//            return
//        }
//
//        /// here we are getting the objects (i.e dictionary) from the json api data ??
//        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//        if let responseJSON = responseJSON as? [String: Any] {
//            print(responseJSON)
//
//            let dict = responseJSON
//            let dataDict = dict["data"] as? NSDictionary
//            print(dataDict?["otp"] ?? "")
//
//            let otp = dataDict?["otp"] ?? 0
//
//            DispatchQueue.main.sync {
//            let otpView = self.storyboard?.instantiateViewController(identifier: "otpView") as! OtpView
//            otpView.otp = otp as? Int
//            self.navigationController?.pushViewController(otpView, animated: true)
//            }
//            //self.verifyOTP(otp: otp as! Int )
//
//        }
//    }
//    task.resume()
//
//    }
}
