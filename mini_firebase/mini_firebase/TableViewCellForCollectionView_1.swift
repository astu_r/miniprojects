//
//  TableViewCellForCollectionView_1.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 15/07/21.
//

import UIKit

class TableViewCellForCollectionView_1: UITableViewCell {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        var collectionView: UICollectionView = {
//                let layout = UICollectionViewFlowLayout()
//                layout.scrollDirection = .vertical
//                //Provide Width and Height According to your need
//                let width = UIScreen.main.bounds.width / 2
//                let height = UIScreen.main.bounds.height
//                layout.itemSize = CGSize(width: width, height: height)
//                //Make Space as Zero
//                layout.minimumInteritemSpacing = 5
//                layout.minimumLineSpacing = 10
//                layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
//                return UICollectionView(frame: frame, collectionViewLayout: layout)
//            }()
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "collectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TableViewCellForCollectionView_1 : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! CollectionViewCell
        if indexPath.row == 0{
        cell.imageCollection.image = UIImage(named: "00")
            cell.collectionLabel.text = "Free home\nsample pickup"
        }
        else if indexPath.row == 1{
            cell.imageCollection.image = UIImage(named: "01")
            cell.collectionLabel.text = "Free consultation\nwith MD doctor"
        }
        else if indexPath.row == 2{
            cell.imageCollection.image = UIImage(named: "10")
            cell.collectionLabel.text = "Online test\nreport"
        }
        else {
            cell.imageCollection.image = UIImage(named: "11")
            cell.collectionLabel.text = "10% cashback\non online payment"
        }
        cell.collectionLabel.font = UIFont.systemFont(ofSize: 12.0)
        cell.collectionLabel.textColor = UIColor.lightGray
        cell.collectionLabel.numberOfLines = 0
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 200, height:80)
//    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            collectionView.layoutIfNeeded()
            collectionView.frame = CGRect(x: 0, y: 0, width: targetSize.width , height: 1)
            return collectionView.collectionViewLayout.collectionViewContentSize
    }
    
}
