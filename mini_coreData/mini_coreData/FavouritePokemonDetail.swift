//
//  FavouritePokemonDetail.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import UIKit
import CoreData

class FavouritePokemonDetail : UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var currentFavourite : Int?
    let singleton = Singleton.shared
    var favourites : [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Details"
        tableView.delegate = self
        tableView.dataSource = self
        singleton.fetchData()
        
        self.favourites = singleton.favourites
        
        let val = (favourites[currentFavourite ?? 0].value(forKey: "image") as? String)!
        let url = URL(string: val )
        let dataImg = try? Data(contentsOf: url! )
        image.image = UIImage(data: dataImg!)
    }
    
    
    @IBAction func update(_ sender: Any) {
        
        self.favourites = singleton.favourites
        let data = favourites[self.currentFavourite ?? 0]
        
        let alert = UIAlertController(title: "Edit details", message: "" , preferredStyle: UIAlertController.Style.alert)
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()
        alert.addTextField()

        let text1 = alert.textFields![0]
        text1.text = data.value(forKey: "name") as? String
        let text2 = alert.textFields![1]
        text2.text = String(data.value(forKey: "moves") as? Int ?? 0)
        let text3 = alert.textFields![2]
        text3.text = String(data.value(forKey: "height") as? Int ?? 0)
        let text4 = alert.textFields![3]
        text4.text = String(data.value(forKey: "weight") as? Int ?? 0)
        
        
        let saveButton = UIAlertAction(title: "save", style: .default) { (action) in
           // self.singleton.updateData(object: self.favourites[self.currentFavourite ?? 0] )
            
            let name = text1.text
            let moves = Int(text2.text ?? "" ) ?? data.value(forKey: "moves") as? Int
            let height = Int(text3.text ?? "" ) ?? data.value(forKey: "height") as? Int
            let weight = Int(text4.text ?? "" ) ?? data.value(forKey: "weight") as? Int
            
            self.singleton.updateData(object: data, name: name!, moves: moves!, height: height!, weight: weight!)
            
          
            self.tableView.reloadData()
            
           
            
            
        }
        
        alert.addAction(saveButton)
        self.present(alert, animated: true, completion: nil)
        
        //singleton.updateData(object: favourites[self.currentFavourite ?? 0] )
    }
    
    @IBAction func remove(_ sender: Any) {
        
        self.favourites = singleton.favourites
        
        let alert = UIAlertController(title: "Confirm Delete ?", message: "" , preferredStyle: UIAlertController.Style.alert)

               // add an action (button)
             //  alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        let submitButton = UIAlertAction(title: "OK", style: .default) { (action) in
            
            self.singleton.deleteData(object: self.favourites[self.currentFavourite ?? 0 ])
            
            let favouritePokemons = self.storyboard?.instantiateViewController(identifier: "favouritePokemons") as! FavouritePokemons
            self.navigationController?.pushViewController(favouritePokemons, animated: true)
        }
        
        alert.addAction(submitButton)
               // show the alert
               self.present(alert, animated: true, completion: nil)
        
        
    }
    
    

}

extension FavouritePokemonDetail : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let index = indexPath.row
        self.favourites = singleton.favourites
        let data = favourites[currentFavourite ?? 0]
        
        if index == 0 {
            cell.textLabel?.text = "NAME"
            cell.detailTextLabel?.text = data.value(forKey: "name") as? String
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)

        }
        else if index == 1{
            cell.textLabel?.text = "No. of Moves"
            cell.detailTextLabel?.text = String(data.value(forKey: "moves") as? Int ?? 0)
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)

        }
        else if index == 2{
            cell.textLabel?.text = "Height"
            cell.detailTextLabel?.text = String(data.value(forKey: "height") as? Int ?? 0)
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)

        }
        else if index == 3{
            cell.textLabel?.text = "Weight"
            cell.detailTextLabel?.text = String(data.value(forKey: "weight") as? Int ?? 0)
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)

        }
        
        return cell
    }
    
    
}
