//
//  ViewController.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 24/06/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSet : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
       // title = "Project D"
       // navigationItem.title = "Project D"
    }

    

}

extension ViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if indexPath.row == 0{
            cell.textLabel?.text = "Marvel Universe"
        }
        if indexPath.row == 1{
            cell.textLabel?.text = "Employee Detail"
        }
        
                //cell.backgroundColor = UIColor.white
              // cell.layer.borderColor = UIColor.gray.cgColor
               // cell.layer.borderWidth = 1
               // cell.layer.cornerRadius = 8
               // cell.clipsToBounds = true
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80.0
    }
    
    /// IS IT CORRECT to reove verything below tableview cells ???
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 460.0
//    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
        
        if indexPath.row == 0 {
            
            let marvelVc = self.storyboard?.instantiateViewController(identifier: "marvelVc") as! MarvelVc
            marvelVc.type = "Marvel"
            self.navigationController?.pushViewController(marvelVc, animated: true)

            
        }
        if indexPath.row == 1{
            
//            let employeeVc = self.storyboard?.instantiateViewController(identifier: "employeeVc") as! EmployeeVc
//
//            self.navigationController?.pushViewController(employeeVc, animated: true)
            let marvelVc = self.storyboard?.instantiateViewController(identifier: "marvelVc") as! MarvelVc
            marvelVc.type = "Employee"
            self.navigationController?.pushViewController(marvelVc, animated: true)
            
        }
        
    }
}

