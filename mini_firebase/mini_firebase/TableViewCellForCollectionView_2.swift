//
//  TableViewCellForCollectionView_2.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 19/07/21.
//

import UIKit

class TableViewCellForCollectionView_2: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension TableViewCellForCollectionView_2 : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell2", for: indexPath) as! CollectionViewCell2
        if indexPath.row == 0{
        cell.imageCollection.image = UIImage(named: "00")
            cell.collectionLabel.text = "Free home\nsample pickup"
        }
        else if indexPath.row == 1{
            cell.imageCollection.image = UIImage(named: "01")
            cell.collectionLabel.text = "Free consultation\nwith MD doctor"
        }
        else if indexPath.row == 2{
            cell.imageCollection.image = UIImage(named: "10")
            cell.collectionLabel.text = "Online test\nreport"
        }
        else {
            cell.imageCollection.image = UIImage(named: "11")
            cell.collectionLabel.text = "10% cashback\non online payment"
        }
        cell.collectionLabel.font = UIFont.systemFont(ofSize: 12.0)
        cell.collectionLabel.textColor = UIColor.lightGray
        cell.collectionLabel.numberOfLines = 0
        return cell
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 200, height:80)
//    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            collectionView.layoutIfNeeded()
            collectionView.frame = CGRect(x: 0, y: 0, width: targetSize.width , height: 1)
            return collectionView.collectionViewLayout.collectionViewContentSize
    }
    
}
