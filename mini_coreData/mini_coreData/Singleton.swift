//
//  Singleton.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import Foundation
import CoreData
import UIKit

class Singleton {
    static let shared = Singleton()
    
    var pokemons : Array<Dictionary<String,String>>?
    var favourites : [NSManagedObject] = []
    
    
    func save(name: String , moves : Int ,  height : Int , weight : Int , image : String) {
      
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      
      // 1
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      // 2
      let entity =
        NSEntityDescription.entity(forEntityName: "Pokemons",
                                   in: managedContext)!
      
      let person = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
      
      // 3
      person.setValue(name, forKeyPath: "name")
      person.setValue(moves, forKeyPath: "moves")
      person.setValue(height, forKeyPath: "height")
      person.setValue(image, forKeyPath: "image")
    
      // 4
      do {
        try managedContext.save()
        //people.append(person)
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }
    
    func fetchData(){
        //1
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext =
          appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
          NSFetchRequest<NSManagedObject>(entityName: "Pokemons")
        
        
        //3
        do {
            favourites = try managedContext.fetch(fetchRequest)
          //print(people)
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func deleteData(object : NSManagedObject){
        
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext =
          appDelegate.persistentContainer.viewContext
        managedContext.delete(object)
        
        do {
          try managedContext.save()
          //people.append(person)
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        
        fetchData()
    }
    
    func updateData(object : NSManagedObject , name: String , moves : Int ,  height : Int , weight : Int ){
        
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext =
          appDelegate.persistentContainer.viewContext
        
      
        object.setValue(name, forKey: "name")
        object.setValue(moves, forKey: "moves")
        object.setValue(height, forKey: "height")
        object.setValue(weight , forKey: "weight")
        
       
        
        do {
          try managedContext.save()
          //people.append(person)
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        
        fetchData()
    }
    
    private init(){}
}
