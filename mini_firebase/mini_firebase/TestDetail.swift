//
//  TestDetail.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 14/07/21.
//

import UIKit

class TestDetail : UIViewController{
    let name = Notification.Name("didReceiveIndex")
    @IBOutlet weak var tableView: UITableView!
    var index = -1
    var data : Array<Dictionary<String,Any>> = [[:]]
    override func viewDidLoad() {
        super.viewDidLoad()
        print("hey im --- \(index)")
        
        if index != -1{
        tableView.delegate = self
        tableView.dataSource = self
        }
        else{
        NotificationCenter.default.addObserver(self, selector: #selector(performed) , name: name, object: nil)
        }
        
        tableView.register(UINib(nibName: "TableViewCellDetail", bundle: nil), forCellReuseIdentifier: "cellDetail")
        
        tableView.register(UINib(nibName: "ImageFull", bundle: nil), forCellReuseIdentifier: "cellImage")
        
        tableView.register(UINib(nibName: "TableViewCellForCollectionView_1", bundle: nil), forCellReuseIdentifier: "cellForCollectionView")
        
        
    }
    
    @objc func performed(){
        tableView.reloadData()
    }
}



extension TestDetail : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
       else if section==1{
            let curr = data[index]
            let detail = curr["detail"] as! Dictionary<String,Any>
            let whatIget = detail["whatWillIget"] as! Array<Dictionary<String,Any>>
        print("count == \(whatIget.count)")
            return whatIget.count
        }
       else if section==2{
        return 5
       }
       else{
        return 2
       }
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "cell", for:indexPath)
        
        guard let cellDetail = tableView.dequeueReusableCell(withIdentifier: "cellDetail" ) as? TableViewCellDetail else{return UITableViewCell()}
        
        guard let cellImage = tableView.dequeueReusableCell(withIdentifier: "cellImage" ) as? ImageFull else{return UITableViewCell()}
        let cellArt = tableView.dequeueReusableCell(withIdentifier: "art") as! TableViewCellArt
        
       
        
        cell.selectionStyle = .none
        cellArt.selectionStyle = .none
        cellDetail.selectionStyle = .none
        cellImage.selectionStyle = .none
        
        let curr = data[self.index]
        let thumbUrl = curr["thumbImageUrl"] as! String
        let thumbTitle = curr["topic"] as! String
        let detail = curr["detail"] as! Dictionary<String,Any>
        let thumbDesc = detail["descNew"] as! String
        let bookings = String(curr["nosBooking"] as! Int) + " Tests"
        let priceActual = curr["labPrice"] as! String
        let priceCut = curr["labOriginalPrice"] as! String
        
        let showHowWillItWork = detail["showHowWillItWork"] as! Bool
        let showWhyChooseDocsAppLab = detail["showWhyChooseDocsAppLab"] as! Bool
        
        print(showHowWillItWork)
        
        if indexPath.section == 0 {
            let url = URL(string: thumbUrl )
            let dataImg = try? Data(contentsOf: url! )
            cellDetail.imageThumb.image = UIImage(data: dataImg!)
            cellDetail.labelThumb.attributedText = addAttributes1(title: thumbTitle , booking: bookings, priceActual: priceActual, priceCut: priceCut, desc: thumbDesc)  //"\n\(thumbTitle)\n\(thumbDesc)"
            cellDetail.labelThumb.numberOfLines = 0
            return cellDetail
        }
        
        if indexPath.section == 1{
            
            if indexPath.row == 0{
                cell.textLabel?.text = "What Will I Get ?"
                cell.detailTextLabel?.text = " "
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
                
                
                return cell
            }
            
            let whatIget = detail["whatWillIget"] as! Array<Dictionary<String,Any>>
            print(indexPath.row)
            let whatIgetDetail = whatIget[indexPath.row-1]
            let labelGet = whatIgetDetail["title"] as! String
            let detailLabelGet = whatIgetDetail["desc"] as! String
            let imageGet = whatIgetDetail["icon"] as! String
            
            cell.textLabel?.text = labelGet
            cell.detailTextLabel?.text = detailLabelGet
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont.systemFont(ofSize: 16.0)
            cell.detailTextLabel?.textColor = UIColor.lightGray
            return cell
        }
        
        if indexPath.section == 2{
            if showHowWillItWork == true{
                
                cellArt.countLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
                cellArt.countLabel.textAlignment = .center
                cellArt.countLabel.layer.cornerRadius = 20.0
                cellArt.contentLabel.layer.cornerRadius = 5.0
                cellArt.contentLabel.numberOfLines = 0
                cellArt.contentLabel.textAlignment = .justified
                
                cellArt.contentLabel.layer.masksToBounds = true
                cellArt.countLabel.layer.masksToBounds = true
                
//                if indexPath.row != 0{
//                let name = "shot" + String(indexPath.row)
//                cellArt.triangleImage.image = UIImage(named: name)
//                }
                
                if indexPath.row == 0{
                    cell.textLabel?.text = "How Does It Work ?"
                    cell.detailTextLabel?.text = "\n It just takes 4 steps to take control of your health"
                    cell.textLabel?.numberOfLines = 0
                    cell.detailTextLabel?.numberOfLines = 0
                    cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
                    cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 14.0)
                    cell.detailTextLabel?.textColor = UIColor.lightGray
                    return cell
                }
               
                if indexPath.row == 1{
                    cellArt.countLabel.text = "1"
                    cellArt.contentLabel.attributedText = addAttributes2(index: indexPath.row, content: "Book Test")
                    cellArt.upperView.isHidden = true
                    
                    return cellArt
                }
                else if indexPath.row == 2{
                    cellArt.countLabel.text = "2"
                    cellArt.contentLabel.attributedText = addAttributes2(index: indexPath.row, content: "Free Home Pickup (biood,urine)")
                   
                    return cellArt
                }
                else if indexPath.row == 3{
                    cellArt.countLabel.text = "3"
                    cellArt.contentLabel.attributedText = addAttributes2(index: indexPath.row, content: "Get report online")
                    
                    return cellArt
                }
                else{
                    cellArt.countLabel.text = "4"
                    cellArt.contentLabel.attributedText = addAttributes2(index: indexPath.row, content: "Free consultation with MD doctor")
                    
                    cellArt.lowerView.isHidden = true
                    return cellArt
                }
            }
            
        }
        
        if indexPath.section == 3{
            
            if indexPath.row == 0 {
                cell.textLabel?.text = "Why Choose DocsApp?"
                cell.detailTextLabel?.text = " "
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
                return cell
            }
            else{
            let collectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cellForCollectionView") as! TableViewCellForCollectionView_1
            
                return collectionViewCell
            }
        }
        
        if indexPath.section == 4{
            
            if indexPath.row == 0 {
                cell.textLabel?.text = "Why Choose DocsApp?"
                cell.detailTextLabel?.text = " "
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
                return cell
            }
            else{
            let collectionViewCell = tableView.dequeueReusableCell(withIdentifier: "cellForCollectionView2") as! TableViewCellForCollectionView_2
            
                return collectionViewCell
            }
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 1{
//           return "What Will I Get "
//        }
//        else {
//            return ""
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 1{
//            return 40.0
//        }
//        return 0.0
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func addAttributes1(title:String , booking:String , priceActual:String , priceCut:String ,desc:String )-> NSMutableAttributedString{
        
        let myString = NSMutableAttributedString()
        
        let titleX = NSAttributedString(string: "\n\(title)\n\n", attributes: [NSAttributedString.Key.font :UIFont.boldSystemFont(ofSize: 20.0) ])
        myString.append(titleX)
        let bookingX = NSAttributedString(string: "\(booking)\n\n", attributes: [NSAttributedString.Key.font :UIFont.systemFont(ofSize: 16.0) ])
        myString.append(bookingX)
        let price = NSAttributedString(string: "₹ \(priceActual) ", attributes: [NSAttributedString.Key.foregroundColor :UIColor.green , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)])
        myString.append(price)
        let priceCutX = NSAttributedString(string: priceCut, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0), NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue , NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        myString.append(priceCutX)
        
        let numb = (1 - Float(priceActual)!/Float(priceCut)!)*100
        let num = Int(numb)
        let off = " " + String(num) + "% off"
        
        let offX = NSAttributedString(string: off, attributes: [NSAttributedString.Key.foregroundColor : UIColor.green , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0)])
        myString.append(offX)
        
        let descX = NSAttributedString(string: "\n\n\(desc)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13.0) , NSAttributedString.Key.foregroundColor : UIColor.lightGray ])
        myString.append(descX)
        print("description - \(desc)")
        return myString
    }
    
    func addAttributes2(index : Int , content : String) -> NSMutableAttributedString {
        
        let font = UIFont(name: "Helvetica", size: 14.0)
            let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = 0.25 * font!.lineHeight
        let attributes = [NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:paragraphStyle]

//            let attributedText = NSAttributedString(string: String, attributes: attributes)
//            self.textView.attributedText = attributedText
        
        let myString = NSMutableAttributedString()
        
        let space = NSAttributedString(string: " ")
       // myString.append(space)
        
        let name = "shot" + String(index)
        
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: name)
        imageAttachment.bounds = CGRect(x: 0, y: -10, width: 30.0, height: 30.0)
        let imageAttach = NSAttributedString(attachment: imageAttachment )
        myString.append(imageAttach)
        
        let contentX = NSAttributedString(string: "    \(content)", attributes: [NSAttributedString.Key.font :UIFont.boldSystemFont(ofSize: 14.0) , NSAttributedString.Key.paragraphStyle:paragraphStyle ])
        
        
        myString.append(contentX)
        
        return myString
    }
    
    
    
}

class TopAlignedLabel: UILabel {
  override func drawText(in rect: CGRect) {
    let textRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
    super.drawText(in: textRect)
  }
}
class PaddingLabel: UILabel {

    // DON'T FORGET TO ADD THIS CLASS NAME TO YOUR LABEL CLASS IN STORYBOARD
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 2 , bottom: 0, right: 0)//CGRect.inset(by:)
        super.drawText(in: rect.inset(by: insets))
        
    }
}


