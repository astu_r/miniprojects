//
//  MarvelVc.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 24/06/21.
//

import UIKit

class MarvelVc: UIViewController {
    
    @IBOutlet weak var tablView: UITableView!
    var type : String?
    var dataSet : Array<Dictionary<String, String>>?
    override func viewDidLoad() {
        super.viewDidLoad()
        tablView.delegate = self
        tablView.dataSource = self
        if type == "Marvel"{
            title = "MARVEL"
        }
        else {
            title = "Employee"
        }
        dataEnter()
        // Do any additional setup after loading the view.
    }
    
   func dataEnter(){
        
        do {
            if let path = Bundle.main.path(forResource: "Property List", ofType: "plist"){
                print("this is ")
                print(path)
                let rotdict = NSDictionary(contentsOfFile: path)
               // let countriesArray  = rotdict?["Countries"] as! Array<Any>
                if type == "Marvel"{
                let marvels  = rotdict?["Marvel"] as! Array<Dictionary<String,String>>
                
                dataSet = marvels
                }
                else{
                    let employee  = rotdict?["Employee"] as! Array<Dictionary<String,String>>
                    
                    dataSet = employee
                }
                
            }
        }

    }
    
    
}

extension MarvelVc : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tablView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let currentCellData = dataSet![indexPath.row]
        
        if type == "Marvel"{
        cell.textLabel?.text = currentCellData["name"]
        cell.detailTextLabel?.text = currentCellData["realname"]
            cell.imageView?.image = UIImage(named: currentCellData["imageurl"]!)
        
        }
        else{
          
            cell.textLabel?.text = currentCellData["employee_name"]
            cell.detailTextLabel?.text = "Age - " + currentCellData["employee_age"]!
            
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if type == "Marvel"{
        return "Marvel Heros"
        }
        else{
            return "Employee"
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if type == "Marvel"{
            
            let marvelDetailVc = self.storyboard?.instantiateViewController(identifier: "marvelDetailVc") as! MarvelDetail
            marvelDetailVc.indexForInfo = indexPath.row
            self.navigationController?.pushViewController(marvelDetailVc, animated: true)
        }
        else {
            let employeeVc = self.storyboard?.instantiateViewController(identifier: "employeeVc") as! EmployeeVc
            employeeVc.indexForInfo = indexPath.row
            self.navigationController?.pushViewController(employeeVc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
