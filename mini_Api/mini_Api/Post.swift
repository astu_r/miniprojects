//
//  Post.swift
//  mini_Api
//
//  Created by Astu Ranjan on 30/06/21.
//

import Foundation

class Post: Codable {
    let employee_salary,employee_age, id: Int
    let employee_name, profile_image: String

    enum CodingKeys: String, CodingKey {
        case id , employee_salary,employee_age , employee_name, profile_image
    }

    init(id: Int,  employee_name: String , employee_salary : Int ,  employee_age : Int, profile_image: String) {
        self.id = id
        self.employee_name = employee_name
        self.employee_age = employee_age
        self.employee_salary = employee_salary
        self.profile_image = profile_image   
    }
}


//            "id": 1,
//            "employee_name": "Tiger Nixon",
//            "employee_salary": 320800,
//            "employee_age": 61,
//            "profile_image": ""
