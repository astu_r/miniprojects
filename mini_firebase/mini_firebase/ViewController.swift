//
//  ViewController.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 13/07/21.
//

import UIKit

class ViewController: UIViewController {
    let name = Notification.Name("didReceiveFirebaseData")
    let activityIndicator = UIActivityIndicatorView()
    @IBOutlet weak var tableView: UITableView!
    var data : Array<Dictionary<String,Any>> = [[:]]
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Labs"
        NotificationCenter.default.addObserver(self, selector: #selector( printData ), name: name , object: nil)
        
        tableView.register(UINib(nibName: "TableViewCellTests", bundle: nil), forCellReuseIdentifier: "cellCustom")
        
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.black
        activityIndicator.style = .large
        
        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        view.addConstraint(horizontalConstraint)
        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        view.addConstraint(verticalConstraint)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    
  @objc func printData(){
    activityIndicator.startAnimating()
        let singleton = Singleton.shared
//        singleton.initalizeRemoteConfig()
//        singleton.fetchRemoteConfigValues()
    self.data = singleton.data
    tableView.delegate = self
    tableView.dataSource = self
    //tableView.reloadData()
    tableView.isHidden = true
    DispatchQueue.main.async {
        
        self.activityIndicator.stopAnimating()
        self.tableView.isHidden = false
        self.tableView.reloadData()
          }
    
    }
    
}

extension ViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.data.count)
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cellCustom", for: indexPath) as? TableViewCellTests else {return UITableViewCell()}
        
       // let cello = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let curr = self.data[indexPath.row]
        let imageLink = curr["bannerImageUrl"]
        let title1_Top = curr["labTitle"] as! String
        let title1_PriceActual = curr["labPrice"] as! String
        let title11_PriceCut = curr["labOriginalPrice"] as! String
        let title1_booking = curr["nosBooking"] as! Int
        let title1_bottomText = "booked this week"
        let title2_bottom = curr["packageIncludes"] as! String
        let detail = curr["detail"] as? Dictionary<String,Any>
        let title2_top = detail?["noOfUsages"] as! String
        
        
        
        let url = URL(string: imageLink as? String ?? "nil" )
        //let dataImg = try? Data(contentsOf: url! )
        
        DispatchQueue.global().async {
        if let dataImg = try? Data(contentsOf: url!) {
            DispatchQueue.main.async {
                cell.imageCell.image = UIImage(data: dataImg)
                cell.imageCell?.reloadInputViews()
                 }
              }
        }
        
        cell.imageAccessory.image = UIImage(named: "accessory")
        
        print(title1_bottomText)
        
//        cello.imageView?.image = UIImage(data: dataImg!)
//        cello.textLabel?.text = title1_Top as? String
//        cello.detailTextLabel?.text = title2_bottom as? String
        cell.labelTop.numberOfLines = 0
        cell.labelBottom.numberOfLines = 0
        cell.labelTop.attributedText = addAttributes1(title: title1_Top, priceActual: title1_PriceActual, priceCut: title11_PriceCut , bookings: title1_booking , bottomText: title1_bottomText)
           
        //"\(title1_Top as! String)\n\(title1_PriceActual as! String) \(title11_PriceCut as! String)"
        
        cell.labelBottom.attributedText = addAttributes2(top: title2_top , bottom: title2_bottom as! String)
        cell.labelBottom.textAlignment = .left
            //"\(title2_top)\n\(title2_bottom as! String)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let testDetail = self.storyboard?.instantiateViewController(identifier: "testDetail") as! TestDetail
        testDetail.index = indexPath.row
        testDetail.data = self.data
        NotificationCenter.default.post( name: Notification.Name("didReceiveIndex"), object: nil)
        self.navigationController?.pushViewController(testDetail, animated: true)
    }
    
    
    
    func addAttributes1 (title : String, priceActual : String, priceCut : String , bookings : Int , bottomText : String) -> NSMutableAttributedString{
        let priceAct = "₹ "+priceActual + " "
        let noBookings = String(bookings) + " " + bottomText
        
        let myString = NSMutableAttributedString()
        let titleX = NSAttributedString(string: "\(title)\n\n", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14)])
        myString.append(titleX)
        let priceActualX = NSAttributedString(string: priceAct, attributes: [NSAttributedString.Key.foregroundColor : UIColor.green , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        myString.append(priceActualX)
        
        let priceCutX = NSAttributedString(string: priceCut, attributes: [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14) , NSAttributedString.Key.foregroundColor : UIColor.lightGray ])
        myString.append(priceCutX)
        
        let numb = (1 - Float(priceActual)!/Float(priceCut)!)*100
        let num = Int(numb)
        let off = " " + String(num) + "% off"
        
        let offX = NSAttributedString(string: off, attributes: [NSAttributedString.Key.foregroundColor : UIColor.green , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        myString.append(offX)
        
        let bottomX = NSAttributedString(string: "\n\n\(noBookings)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12)])
        myString.append(bottomX)
        
        return myString
    }
    
    func addAttributes2(top : String , bottom : String) -> NSMutableAttributedString{
        
        let myString = NSMutableAttributedString()
        
        let topX = NSAttributedString(string: "\n\(top)\n\n", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12) , NSAttributedString.Key.foregroundColor : UIColor.gray])
        myString.append(topX)
        
        let bottomX = NSAttributedString(string: bottom, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray , NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10) ])
        myString.append(bottomX)
        
        return myString
    }
}

