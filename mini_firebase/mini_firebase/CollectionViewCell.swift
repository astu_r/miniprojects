//
//  CollectionViewCell.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 15/07/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageCollection: UIImageView!
    
    @IBOutlet weak var collectionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    
}
