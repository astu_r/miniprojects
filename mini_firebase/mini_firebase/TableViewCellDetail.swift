//
//  TableViewCellDetail.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 14/07/21.
//

import UIKit

class TableViewCellDetail: UITableViewCell {

    @IBOutlet weak var imageThumb: UIImageView!
    @IBOutlet weak var labelThumb: UILabel!
    
    
}
