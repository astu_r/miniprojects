//
//  ViewController.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Core Data"
    }

    @IBAction func pokemons(_ sender: Any) {
        let allPokemons = self.storyboard?.instantiateViewController(identifier: "allPokemons") as! AllPokemons
        self.navigationController?.pushViewController(allPokemons, animated: true)
    }
    
    @IBAction func favourites(_ sender: Any) {
        let favouritePokemons = self.storyboard?.instantiateViewController(identifier: "favouritePokemons") as! FavouritePokemons
        self.navigationController?.pushViewController(favouritePokemons, animated: true)
    }
    
    
}

//let marvelVc = self.storyboard?.instantiateViewController(identifier: "marvelVc") as! MarvelVc
//marvelVc.type = "Marvel"
//self.navigationController?.pushViewController(marvelVc, animated: true)
