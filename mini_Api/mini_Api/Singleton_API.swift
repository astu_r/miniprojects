//
//  Singleton_otpPost.swift
//  mini_Api
//
//  Created by Astu Ranjan on 02/07/21.
//testing


import Foundation

class Singleton_API  {
    
   static let shared = Singleton_API()
    
    var otp : Int?
    var isOtpVerified : Bool = false
    func getOtp(num : Any) {
        
        let json: [String: Any] = ["phone": num as Any]
       //testing 2
    /// convert dictionary to json data ??
    let jsonData = try? JSONSerialization.data(withJSONObject: json)

    // create post request
    
        let url = URL(string: ("http://52.66.119.84:2025/api/v1/login/user?debug=true").replacingOccurrences(of: " ", with: "%20"))!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")

    // insert json data to the request
    request.httpBody = jsonData
        
        
        
        
    
    /// the data is the data we have got in the response to the post request ???
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            print(error?.localizedDescription ?? "No data")
            return
        }
        
        /// here we are getting the objects (i.e dictionary) from the json api data ??
        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        if let responseJSON = responseJSON as? [String: Any] {
            print(responseJSON)
            
            let dict = responseJSON
            let dataDict = dict["data"] as? NSDictionary
            print(dataDict?["otp"] ?? "")
            let temp : Int?
            temp = dataDict?["otp"] as? Int
            self.otp = temp
            

            
        }
    }
    task.resume()
        
    }
    
    func verifyOTP(otp:Int){
        let json: [String: Any] = ["phone": "9490449444","otp":"\(otp)"]

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        // create post request
        let url = URL(string: ("http://52.66.119.84:2025/api/v1/verify/otp").replacingOccurrences(of: " ", with: "%20"))!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // insert json data to the request
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { [self] data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                 
                //self.moveToNextVc()
                
                isOtpVerified.self = true
            }
        }
        task.resume()
    }
    private init (){}
    
}
