//
//  EmployeeVc.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 24/06/21.
//

import UIKit

class EmployeeVc: UIViewController {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var indexForInfo : Int?
    var dataSet : Array<Dictionary<String, String>>?
    var map = [ 0:"id", 1:"employee_name", 2:"employee_salary", 3:"employee_age"]
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Employee Detail"
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "EmployeeTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        dataEnter()
      
    }
    
    func dataEnter(){
        
        do {
            if let path = Bundle.main.path(forResource: "Property List", ofType: "plist"){
                let rotdict = NSDictionary(contentsOfFile: path)
               // let countriesArray  = rotdict?["Countries"] as! Array<Any>
                let employee  = rotdict?["Employee"] as! Array<Dictionary<String, String>>?
                
                dataSet = employee
                
            }
        }

    }
}

extension EmployeeVc : UITableViewDelegate ,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? EmployeeTableViewCell else {return UITableViewCell()}
        let value = dataSet![indexForInfo ?? 0]
        let index = indexPath.row
        
        if index == 0 {
            cell.employeeLabel1.text = "ID"
            cell.employeeLabel2.text =  value[map[indexPath.row]!]!
        }
        if index == 1 {
            cell.employeeLabel1.text = "Employee Name"
            cell.employeeLabel2.text =  value[map[indexPath.row]!]!
        }
        if index == 2 {
            cell.employeeLabel1.text = "Age"
            cell.employeeLabel2.text =  value[map[indexPath.row]!]!
        }
        if index == 3 {
            cell.employeeLabel1.text = "Monthly Salary"
            cell.employeeLabel2.text =  value[map[indexPath.row]!]!
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        80.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
