//
//  ViewController.swift
//  mini_Api
//
//  Created by Astu Ranjan on 30/06/21.
//

import UIKit

class OtpView : UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var txtOTP6: UITextField!
    @IBOutlet weak var txtOTP5: UITextField!
    @IBOutlet weak var txtOTP4: UITextField!
    @IBOutlet weak var txtOTP3: UITextField!
    @IBOutlet weak var txtOTP2: UITextField!
    @IBOutlet weak var txtOTP1: UITextField!
    
    @IBOutlet weak var lblTimer: UILabel!
    
    @IBOutlet weak var resend: UIButton!
    
    
    
    var otp : Int?
    
    var timer: Timer?
    var totalTime = 60
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       // navigationController?.isNavigationBarHidden = true
        
        txtOTP1.backgroundColor = UIColor.clear
        txtOTP2.backgroundColor = UIColor.clear
        txtOTP3.backgroundColor = UIColor.clear
        txtOTP4.backgroundColor = UIColor.clear
        txtOTP5.backgroundColor = UIColor.clear
        txtOTP6.backgroundColor = UIColor.clear
        
        txtOTP1.keyboardType = .numberPad
        txtOTP2.keyboardType = .numberPad
        txtOTP3.keyboardType = .numberPad
        txtOTP4.keyboardType = .numberPad
        txtOTP5.keyboardType = .numberPad
        txtOTP6.keyboardType = .numberPad
        
        
        addBottomBorderTo(textField: txtOTP1)
        addBottomBorderTo(textField: txtOTP2)
        addBottomBorderTo(textField: txtOTP3)
        addBottomBorderTo(textField: txtOTP4)
        addBottomBorderTo(textField: txtOTP5)
        addBottomBorderTo(textField: txtOTP6)
        
        txtOTP1.delegate = self
        txtOTP2.delegate = self
        txtOTP3.delegate = self
        txtOTP4.delegate = self
        txtOTP5.delegate = self
        txtOTP6.delegate = self
        
        txtOTP1.becomeFirstResponder()
        
        startOtpTimer()
        
        // create the alert
        let alert = UIAlertController(title: "OTP", message: String(otp ?? 0) , preferredStyle: UIAlertController.Style.alert)

               // add an action (button)
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

               // show the alert
               self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func resendAction(_ sender: Any) {
        if timer == nil{
            
            let alert = UIAlertController(title: "OTP", message: String(otp ?? 0) , preferredStyle: UIAlertController.Style.alert)
                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
            startOtpTimer()
        }
    }


    func addBottomBorderTo(textField:UITextField) {
        let layer = CALayer()
        layer.backgroundColor = UIColor.gray.cgColor
        layer.frame = CGRect(x: 0.0, y: textField.frame.size.height - 2.0, width: textField.frame.size.width, height: 2.0)
        textField.layer.addSublayer(layer)
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((textField.text?.count)! < 1 ) && (string.count > 0) {
            if textField == txtOTP1 {
                txtOTP2.becomeFirstResponder()
            }
            if textField == txtOTP2 {
                txtOTP3.becomeFirstResponder()
            }
            
            if textField == txtOTP3 {
                txtOTP4.becomeFirstResponder()
            }
            if textField == txtOTP4 {
                txtOTP5.becomeFirstResponder()
            }
            if textField == txtOTP5 {
                txtOTP6.becomeFirstResponder()
            }
            if textField == txtOTP6 {
                txtOTP4.resignFirstResponder()
            }
            
            textField.text = string
            return false
        } else if ((textField.text?.count)! >= 1) && (string.count == 0) {
            if textField == txtOTP2 {
                txtOTP1.becomeFirstResponder()
            }
            if textField == txtOTP3 {
                txtOTP2.becomeFirstResponder()
            }
            if textField == txtOTP4 {
                txtOTP3.becomeFirstResponder()
            }
            if textField == txtOTP5 {
                txtOTP4.becomeFirstResponder()
            }
            if textField == txtOTP6 {
                txtOTP5.becomeFirstResponder()
            }
            if textField == txtOTP1 {
                txtOTP1.resignFirstResponder()
            }
            
            textField.text = ""
            return false
        } else if (textField.text?.count)! >= 1 {
            textField.text = string
            return false
        }
        
        return true
    }
    
    
    
    
       
        private func startOtpTimer() {
            resend.isHidden = true
               self.totalTime = 10
               self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
           }
       
       @objc func updateTimer() {
               print(self.totalTime)
         // will show timer
        self.lblTimer.text = String(self.totalTime)
    
               if totalTime != 0 {
                   totalTime -= 1  // decrease counter timer
               } else {
                   if let timer = self.timer {
                       timer.invalidate()
                       self.timer = nil
                    resend.isEnabled = true
                    resend.titleLabel?.text = "Resend"
                    resend.titleLabel?.textColor = .black
                   
                    resend.isHidden  = false
                    resend.setTitle("Resend", for: .normal)
                    
                   }
               }
           }
       func timeFormatted(_ totalSeconds: Int) -> String {
           let seconds: Int = totalSeconds % 60
           let minutes: Int = (totalSeconds / 60) % 60
           return String(format: "%02d:%02d", minutes, seconds)
       }
    
    
    
    
    
    
    
    
    @IBAction func checkOtp(_ sender: Any) {
        
        if txtOTP1.text == "" || txtOTP2.text == "" || txtOTP3.text == "" || txtOTP4.text == "" || txtOTP5.text == "" || txtOTP6.text == "" {
            return
        }
        
        var enteredOtpString = String(txtOTP1.text!) + String(txtOTP2.text!) + String(txtOTP3.text!) + String(txtOTP4.text!) + String(txtOTP5.text!) + String(txtOTP6.text!)
        
        let enteredOtpNum = Int(enteredOtpString) ?? 0
        print("entered otp is ====== \(enteredOtpNum)")
        
        let apiCall = Singleton_API.shared
        apiCall.verifyOTP(otp: enteredOtpNum)
        sleep(1)
        if apiCall.isOtpVerified {
            moveToNextVc()
        }
    }
    
    func moveToNextVc() {

        let employee = self.storyboard?.instantiateViewController(identifier: "employeeVc") as! Employee
        self.navigationController?.pushViewController(employee, animated: true)
        
    }
    
}


