//
//  AllPokemons.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import UIKit

class AllPokemons : UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var names : [String] = []
    var filterResult : [String] = []
    var nameIndexMapping : [String:Int] = [:]
    
    let temp = Singleton.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Pokemons"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        title = "Pokemons"
        tableView.delegate = self
        tableView.dataSource = self
        
        getData()
    }
    
    @IBAction func home(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "viewController") as! ViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getData() -> Void {
        
        let session = URLSession.shared
        let seviceURL = URL(string:"https://pokeapi.co/api/v2/pokemon")
        let task  = session.dataTask(with:seviceURL!){(serviceData,serviceResponse,error) in
        if(error == nil){
        let httpsResponse = serviceResponse as! HTTPURLResponse
           if httpsResponse.statusCode == 200{
        let jsonData = try? JSONSerialization.jsonObject(with:serviceData!,options:.mutableContainers)
            
            let data = jsonData as? Dictionary<String,Any>
            let pokemons = data?["results"] as? Array<Dictionary<String, String>> /// Array of dictionary
            
            for i in 0..<pokemons!.count{
                let val = pokemons?[i] as Dictionary<String , String>
                let valName = val["name"] ?? "nil"
                self.names.append(valName)
                self.nameIndexMapping[valName] = i
            }
           
            self.temp.pokemons = pokemons
            
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
            
//        print(jsonData)
//
//
//            let array = jsonData as! Array<Any>
//            print(array)
//            let dict = array[4] as? NSDictionary
//            print(dict?["body"]!)
            
                  }
            
             }
       }
        task.resume()

    }

    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }

    func filterContentForSearchText(_ searchText: String
                                   ) {
     filterResult = names.filter { (names: String) -> Bool in
        return names.lowercased().contains(searchText.lowercased())
        //candy.name.lowercased().contains(searchText.lowercased())
      }
      tableView.reloadData()
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }

}

extension AllPokemons : UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
    
  }
}


extension AllPokemons : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filterResult.count
          }
        else{
            return self.temp.pokemons?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let data = self.temp.pokemons
        let pokemon = data?[indexPath.row]
        
        
        if isFiltering{
            
            cell.textLabel?.text = filterResult[indexPath.row]
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            
            let index = nameIndexMapping[filterResult[indexPath.row]] ?? 0
            
           
        }
        else{
        cell.textLabel?.text = pokemon?["name"]
        cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pokemonDetail = self.storyboard?.instantiateViewController(identifier: "pokemonDetail") as! PokemonDetail
        if isFiltering{
            var index = nameIndexMapping[filterResult[indexPath.row]] ?? 0
            pokemonDetail.currentPokemon = index + 1
        }
        else{
        pokemonDetail.currentPokemon = indexPath.row + 1
        }
        self.navigationController?.pushViewController(pokemonDetail, animated: true)
    }
    
    
}
