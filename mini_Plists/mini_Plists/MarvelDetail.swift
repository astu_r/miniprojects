//
//  MarvelDetail.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 25/06/21.
//

import UIKit

class MarvelDetail : UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var indexForInfo : Int?
    var dataSet : Array<Dictionary<String, String>>?
    var map = [ 1:"team", 2:"firstappearance",  3:"createdby" , 4: "publisher"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "MarvelTableViewCell_1", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "MarvelTableViewCell_2", bundle: nil), forCellReuseIdentifier: "cell2")
        tableView.register(UINib(nibName: "MarvelTableViewCell_3", bundle: nil), forCellReuseIdentifier: "cell3")
        
        dataEnter()
    }
    
    func dataEnter(){
        
        do {
            if let path = Bundle.main.path(forResource: "Property List", ofType: "plist"){
                let rotdict = NSDictionary(contentsOfFile: path)
               // let countriesArray  = rotdict?["Countries"] as! Array<Any>
                let marvel  = rotdict?["Marvel"] as! Array<Dictionary<String, String>>?
                
                dataSet = marvel
                
            }
        }

    }
}

extension MarvelDetail : UITableViewDelegate ,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
       let finalCell = UITableViewCell()
        let value = dataSet![indexForInfo ?? 0]
       
        if index == 0{
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MarvelTableViewCell_1 else {return UITableViewCell()}
            cell.imageProfile.image = UIImage(named: value["imageurl"]!)
           
//            cell.labelTop.text = value["name"]
//            cell.labelBottom.text = value["realname"]
            cell.selectionStyle = .none
            return cell
        }
        
        if index == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellP", for: indexPath)
            cell.textLabel?.text = value["name"]
            cell.detailTextLabel?.text = value["realname"]
            cell.backgroundColor = .lightGray
            cell.selectionStyle = .none
            return cell
        }
        
        if index >= 2 && index <= 5 {
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? MarvelTableViewCell_2 else {return UITableViewCell()}
            
            
            if index == 2{
                cell.marvelLabel1.text = "Team"
                cell.marvelLabel2.text = value["team"]
            }
            if index == 3{
                cell.marvelLabel1.text = "First Appearance"
                cell.marvelLabel2.text = value["firstappearance"]
            }
            if index == 4{
                cell.marvelLabel1.text = "Created By"
                cell.marvelLabel2.text = value["createdby"]
            }
            if index == 5{
                cell.marvelLabel1.text = "Publisher"
                cell.marvelLabel2.text = value["publisher"]
            }
            cell.selectionStyle = .none
            return cell
        }
        
        if index == 6{
            
            guard  let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as? MarvelTableViewCell_3 else {return UITableViewCell()}
            
            cell.bioLabelTop.text = "Bio"
            cell.bioLabelBottom.text = value["bio"]
            cell.selectionStyle = .none
            
          //  cell.bioLabelBottom.lineBreakMode = UILineBreakModeWordWrap  ; , NSLineBreakByTruncatingTail
            
            return cell
            
        }
        
        
        return finalCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200.0
        }
        else if indexPath.row == 6{
            return 300.0
        }
        else {
            return 80.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    
}

class PaddingLabel: UILabel {

    // DON'T FORGET TO ADD THIS CLASS NAME TO YOUR LABEL CLASS IN STORYBOARD
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 15 , bottom: 0, right: 0)//CGRect.inset(by:)
        super.drawText(in: rect.inset(by: insets))
        
    }
}
