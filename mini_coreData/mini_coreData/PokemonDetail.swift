//
//  PokemonDetail.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import UIKit

class PokemonDetail : UIViewController{
    
    

    @IBOutlet weak var imagePokemon: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var currentPokemon : Int?
    var name : String?
    var moves : Int?
    var height : Int?
    var weight : Int?
    var image : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        title = "Details"
        
        getData()
    }
    
    @IBAction func addToFavourites(_ sender: Any) {
        let pokemon = Singleton.shared
        
        pokemon.save(name: self.name! , moves: self.moves!, height: self.height!, weight: self.weight!, image: self.image!)
        
        let favouritePokemons = self.storyboard?.instantiateViewController(identifier: "favouritePokemons") as! FavouritePokemons
        self.navigationController?.pushViewController(favouritePokemons, animated: true)
        
    }
    
    func getData() -> Void {
        
        var url = "https://pokeapi.co/api/v2/pokemon/"
        url = url + String(currentPokemon ?? 1)
        let session = URLSession.shared
        let seviceURL = URL(string:url)
        let task  = session.dataTask(with:seviceURL!){ [self](serviceData,serviceResponse,error) in
        if(error == nil){
        let httpsResponse = serviceResponse as! HTTPURLResponse
           if httpsResponse.statusCode == 200{
        let jsonData = try? JSONSerialization.jsonObject(with:serviceData!,options:.mutableContainers)
            
            let data = jsonData as? Dictionary<String,Any>
           
            
            self.name = data?["name"] as? String
            self.height = data?["height"] as? Int
            self.weight = data?["weight"] as? Int
            
            let arr = data?["moves"] as? Array<Any>
            self.moves = arr?.count
            
            let sprites = data?["sprites"] as? Dictionary<String , Any>
            
            self.image = sprites?["front_default"] as? String
            
            let url = URL(string: self.image! )
            let dataImg = try? Data(contentsOf: url! )
           // imagePokemon.image = UIImage(data: dataImg!)
//            print(self.name!)
//            print(self.moves!)
//            print(self.image!)
            
            DispatchQueue.main.sync {
                self.tableView.reloadData()
                imagePokemon.image = UIImage(data: dataImg!)
            }
//        print(jsonData)
//
//
//            let array = jsonData as! Array<Any>
//            print(array)
//            let dict = array[4] as? NSDictionary
//            print(dict?["body"]!)
            
                  }
            
             }
       }
        task.resume()

    }
    
   

}



extension PokemonDetail : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let index = indexPath.row

        if index == 0 {
            cell.textLabel?.text = "NAME"
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)
            cell.detailTextLabel?.text = self.name
            
        }
        else if index == 1{
            cell.textLabel?.text = "No. of Moves"
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)
            cell.detailTextLabel?.text = String(self.moves ?? 0)
        }
        else if index == 2{
            cell.textLabel?.text = "Height"
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)
            cell.detailTextLabel?.text = String(self.height ?? 0)
        }
        else if index == 3{
            cell.textLabel?.text = "Weight"
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            cell.detailTextLabel?.font = .systemFont(ofSize: 20.0)
            cell.detailTextLabel?.text = String(self.weight ?? 0)
        }
        
        return cell
    }
    
    
    
    
}
