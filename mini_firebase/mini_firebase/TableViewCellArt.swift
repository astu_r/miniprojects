//
//  TableViewCell.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 15/07/21.
//

import UIKit

class TableViewCellArt : UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var triangleImage: UIImageView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var lowerView: UIView!
    
}
