//
//  EmployeeTableViewCell.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 25/06/21.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var employeeLabel1: UILabel!
    
    @IBOutlet weak var employeeLabel2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
