//
//  Employee.swift
//  mini_Api
//
//  Created by Astu Ranjan on 30/06/21.
//

import UIKit

class Employee : UIViewController{

   
    @IBOutlet weak var tableView: UITableView!
    let activityIndicator = UIActivityIndicatorView()
    let searchController = UISearchController(searchResultsController: nil)
    var result = Array<Dictionary<String,Any>>()
    var names : [String] = []
    var filterResult : [String] = []
    var nameIndexMapping : [String:Int] = [:]
    
   // var result = [Post]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Employees"
        navigationItem.searchController = searchController

        definesPresentationContext = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.black
        activityIndicator.style = .large
        
        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        view.addConstraint(horizontalConstraint)
        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        view.addConstraint(verticalConstraint)
        
        getData()
       // loadData()
        
    }
    
    @IBAction func refresh(_ sender: Any) {
        getData()
    }
    
    

    
    func getData() -> Void {
        
        activityIndicator.startAnimating()
        tableView.isHidden = true
        let session = URLSession.shared
        let seviceURL = URL(string:"http://dummy.restapiexample.com/api/v1/employees")
        let task  = session.dataTask(with:seviceURL!){ [self](serviceData,serviceResponse,error) in
        if(error == nil){
        let httpsResponse = serviceResponse as! HTTPURLResponse
           if httpsResponse.statusCode == 200{
        let jsonData = try? JSONSerialization.jsonObject(with:serviceData!,options:.mutableContainers)
            
            let dict = jsonData as! NSDictionary
            result.self = dict["data"] as! [[String : Any]]
            print(result.count)
            if result.count > 0 && names.count == 0{
            for i in 0..<result.count {
                let post = self.result[i]
                if post["employee_name"] != nil {
                let emp_name = post["employee_name"] as! String
                names.append(emp_name)
                nameIndexMapping[emp_name] = i
                }
              }
            }
            
                        
            DispatchQueue.main.async {
                activityIndicator.stopAnimating()
                tableView.isHidden = false
                self.tableView.reloadData()
                  }
         }
       }
     }
        task.resume()

    }
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }

    func filterContentForSearchText(_ searchText: String
                                   ) {
     filterResult = names.filter { (names: String) -> Bool in
        return names.lowercased().contains(searchText.lowercased())
        //candy.name.lowercased().contains(searchText.lowercased())
      }
      tableView.reloadData()
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
}

struct employee : Codable{
    let status ,message : String
    var detail = [Post]()
    
}

//struct Post : Codable {
//    let employee_salary,employee_age, id: Int
//    let employee_name, profile_image: String
//
//    enum CodingKeys: String, CodingKey {
//        case id , employee_salary,employee_age , employee_name, profile_image
//    }
//}

extension Employee : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.result.count)
        if isFiltering {
            return filterResult.count
          }
        return self.result.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath)
        let post = self.result[indexPath.row]
        
        if isFiltering{
            
            cell.textLabel?.text = filterResult[indexPath.row]
            var index = nameIndexMapping[filterResult[indexPath.row]] ?? 0
            let data = self.result[index]
            cell.detailTextLabel?.text = String(data["employee_salary"] as! Int)
           
        }
        else {
        
        cell.textLabel?.text = post["employee_name"] as! String
        cell.detailTextLabel?.text = String(post["employee_salary"] as! Int)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let employeeDetail = self.storyboard?.instantiateViewController(identifier: "employeeDetail") as! EmployeeDetail
        if isFiltering{
            var index = nameIndexMapping[filterResult[indexPath.row]] ?? 0
            employeeDetail.index = index + 1
        }
        else{
        employeeDetail.index = indexPath.row + 1
        }
        print("index passed ==== \(employeeDetail.index)")
        self.navigationController?.pushViewController(employeeDetail , animated: true)
    }
    
    
}

extension Employee: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
    
  }
}



/*
    func loadData() -> Void {
      let session = URLSession.shared
        let url = URL(string: "http://dummy.restapiexample.com/api/v1/employees")!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            // Check the response
           // print(response!)
            
            // Check if an error occured
            if error != nil {
                // HERE you can manage the error
                print(error!)
                return
            }
            
            // Serialize the data into an object
            do {
                let json = try JSONDecoder().decode(employee.self, from: data! )
                    //try JSONSerialization.jsonObject(with: data!, options: [])
                self.result = json.detail
                print(self.result.count)
//                for i in 0...0{
//                    let post = self.result[i]
//                    print(post.body)
//                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                
                
            } catch {
                print("Error during JSON serialization: \(error.localizedDescription)")
            }
            
        })
        task.resume()
    
    }
    */
