//
//  UserDefaultDataSave.swift
//  mini_Plists
//
//  Created by Astu Ranjan on 25/06/21.
//


import UIKit

class UserDefaultDataSave : UIViewController {
    
    @IBOutlet weak var labelData: UITextField!
   let keyName = "count"
   
   
    //var array : Array<String>?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()

        
    }
    

    
    @IBAction func buttonSace(_ sender: Any) {
        var num : Int = 0
        if var numCheck = UserDefaults.standard.object(forKey: keyName){
            num = numCheck as! Int
        }
        else{
            UserDefaults.standard.setValue(0 , forKey:keyName )
            UserDefaults.standard.synchronize()
            num = 0
        }
        num += 1
        var data = labelData.text
        
        if data != ""{
        UserDefaults.standard.setValue(data , forKey:String(num) )
        UserDefaults.standard.setValue(num , forKey:keyName )
        UserDefaults.standard.synchronize()
        labelData.text = .none
        tableView.reloadData()
        }
        else {
            tableView.reloadData()
        }
        
    }
    
    
    
}
//let x = UserDefaults.standard.object(forKey: "key1") as! String

extension UserDefaultDataSave : UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var num = UserDefaults.standard.object(forKey: keyName) as? Int
        return num ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        let x = UserDefaults.standard.object(forKey: String(indexPath.row + 1)) as! String
        cell.textLabel?.text = x
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Saved Data"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
