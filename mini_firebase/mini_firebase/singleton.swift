//
//  singleton.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 13/07/21.
//

import Foundation
import FirebaseRemoteConfig

class Singleton {
    
     static var shared = Singleton()
    
    private var remoteConfig : RemoteConfig?
    
    var data : Array<Dictionary< String,Any >> = [[:]]
    
    func initalizeRemoteConfig() {
            remoteConfig = RemoteConfig.remoteConfig()
           // remoteConfig?.setDefaults(fromPlist:"RemoteConfigDefaults")
            let settings = RemoteConfigSettings()
            settings.minimumFetchInterval = 0
            remoteConfig?.configSettings = settings
    }
    
    func fetchRemoteConfigValues() {
            #if DEBUG
            let fetchDuration: TimeInterval = 0
            #else
            let fetchDuration: TimeInterval = 86400
            #endif
        self.remoteConfig?.fetchAndActivate(completionHandler: { [self]
                status, error in
                if status == .successFetchedFromRemote || status == .successUsingPreFetchedData {
                        
                    let employees = RemoteConfig.remoteConfig().configValue(forKey: "Employees").jsonValue
                    
                    let employee = employees as!  Dictionary< String, Any>
                   // print(employee["employee_data"] ?? "null")
                    let emp = employee["employee_data"] as! Array<Dictionary<String, Any>>
                   // print("employee 1 ==== \(emp[0] )")
                    let name = emp[0]
                    print("name : \(name["employee_name"]!)")
                    
                    let labs_data = RemoteConfig.remoteConfig().configValue(forKey: "labs_data").jsonValue
                    self.data = labs_data as! Array<Dictionary< String,Any >>
                   //print(data[0])
                    NotificationCenter.default.post( name: Notification.Name("didReceiveFirebaseData"), object: nil)
                    
                }
        })
    }
    
    
    private init(){}
}
