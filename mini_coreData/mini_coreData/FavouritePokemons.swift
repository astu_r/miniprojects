//
//  FavouritePokemons.swift
//  mini_coreData
//
//  Created by Astu Ranjan on 07/07/21.
//

import UIKit
import CoreData

class FavouritePokemons : UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var names : [String] = []
    var filterResult : [String] = []
    var nameIndexMapping : [String:Int] = [:]
    
    let singleton = Singleton.shared
    var favourites : [NSManagedObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Pokemons"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        tableView.delegate = self
        tableView.dataSource = self
        title = "Favourites"
        
        singleton.fetchData()
        loadSearch()
       
    }

    @IBAction func home(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "viewController") as! ViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func loadSearch(){
        
        self.favourites = singleton.favourites
        
        for i in 0..<favourites.count{
            let temp = favourites[i].value(forKey: "name") as? String
            names.append(temp!)
            nameIndexMapping[temp!] = i
        }
        
    }
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }

    func filterContentForSearchText(_ searchText: String
                                   ) {
     filterResult = names.filter { (names: String) -> Bool in
        return names.lowercased().contains(searchText.lowercased())
        //candy.name.lowercased().contains(searchText.lowercased())
      }
      tableView.reloadData()
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
}

extension FavouritePokemons : UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
    
  }
}

extension FavouritePokemons : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return filterResult.count
          }
        else{
           return singleton.favourites.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        self.favourites = singleton.favourites
        
        if isFiltering{
            
            cell.textLabel?.text = filterResult[indexPath.row]
            cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
            
            let index = nameIndexMapping[filterResult[indexPath.row]] ?? 0
        }
        else{
            
        cell.textLabel?.text = favourites[indexPath.row].value(forKey: "name") as? String
        cell.textLabel?.font = .boldSystemFont(ofSize: 20.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let favouritePokemonDetail = self.storyboard?.instantiateViewController(identifier: "favouritePokemonDetail") as! FavouritePokemonDetail
        if isFiltering{
            favouritePokemonDetail.currentFavourite = nameIndexMapping[filterResult[indexPath.row]]
        }
        else{
        favouritePokemonDetail.currentFavourite = indexPath.row
        }
        self.navigationController?.pushViewController(favouritePokemonDetail, animated: true)
    }
    
    
    
}
