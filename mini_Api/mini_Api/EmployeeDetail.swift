//
//  EmployeeDetail.swift
//  mini_Api
//
//  Created by Astu Ranjan on 01/07/21.
//

import UIKit

class EmployeeDetail : UIViewController {
    
    let activityIndicator = UIActivityIndicatorView()
    var index : Int?
    var result = Dictionary<String,Any>()
    //var resultX = Array<Dictionary<String,Any>>()
    @IBOutlet weak var tableView : UITableView!
    
    
    @IBOutlet weak var image: UIImageView! {
        didSet{
            image.layer.cornerRadius = 90
            image.backgroundColor = UIColor.red
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        image.image = UIImage(named: "Profile")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.black
        activityIndicator.style = .large
        
        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        view.addConstraint(horizontalConstraint)
        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        view.addConstraint(verticalConstraint)
        
        getData()
    }
    
    @IBAction func refresh(_ sender: Any) {
        getData()
    }
    
    
    func getData() -> Void {
        
        activityIndicator.startAnimating()
        tableView.isHidden = true
        let session = URLSession.shared
        
        var finalIndex = 1
        if let indX = index {
            finalIndex = indX
        }
        
        let url = "https://dummy.restapiexample.com/api/v1/employee/" + String(finalIndex  )
        let seviceURL = URL(string:url)
        let task  = session.dataTask(with:seviceURL!){ [self](serviceData,serviceResponse,error) in
        if(error == nil){
        let httpsResponse = serviceResponse as! HTTPURLResponse
           if httpsResponse.statusCode == 200{
        let jsonData = try? JSONSerialization.jsonObject(with:serviceData!,options:.mutableContainers)
            
            let dict = jsonData as! NSDictionary
            result.self = dict["data"] as! [String:Any]
            print(" RESULT COUNT IS ---- \(result.count)")
            
            DispatchQueue.main.async {
                activityIndicator.stopAnimating()
                tableView.isHidden = false
                self.tableView.reloadData()
                 }
            
            }
            
         }
      }
        task.resume()

  }
    
}

extension EmployeeDetail : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath)
        
        
        
        let ind = indexPath.row
        
        if ind == 0{
            cell.textLabel?.text = "Name"
            cell.detailTextLabel?.text = result["employee_name"] as? String
        }
        
        if ind == 1{
            cell.textLabel?.text = "Salary"
            cell.detailTextLabel?.text = String(result["employee_salary"] as? Int ?? 0)

        }
        
        if ind == 2{
            cell.textLabel?.text = "Age"
            cell.detailTextLabel?.text = String(result["employee_age"] as? Int ?? 0)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor? = .red
        //view.frame = CGRect(x: 0.0, y: 0.0, width: 100, height: 50)
        return view
    }
}
