//
//  CollectionViewCell2.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 19/07/21.
//

import UIKit

class CollectionViewCell2: UICollectionViewCell {
    @IBOutlet weak var imageCollection: UIImageView!
    
    @IBOutlet weak var collectionLabel: UILabel!
    
}
