//
//  TableViewCellTests.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 14/07/21.
//

import UIKit

class TableViewCellTests: UITableViewCell {

   
   
    
    
    @IBOutlet weak var labelTop: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelBottom: UILabel!
    
    @IBOutlet weak var imageAccessory: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
