//
//  ImageFull.swift
//  mini_firebase
//
//  Created by Astu Ranjan on 14/07/21.
//

import UIKit

class ImageFull: UITableViewCell {

    @IBOutlet weak var imageFull: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
